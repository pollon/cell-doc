module Main where

import Distribution.Simple
import Distribution.Simple.Setup (BuildFlags)
import Distribution.PackageDescription (HookedBuildInfo, emptyHookedBuildInfo)
import System.Process (runCommand,  waitForProcess )

main :: IO ()
main = defaultMainWithHooks $ simpleUserHooks { preBuild = genCSS }

genCSS :: Args -> BuildFlags -> IO HookedBuildInfo
genCSS _ _ = do
    waitForProcess =<< runCommand "cell . > resources/style.new;\
    \echo -e '\n-- CSS Diff --\n';diff resources/style.css resources/style.new;\
		\echo -e '\n';mv resources/style.new resources/style.css"
    return emptyHookedBuildInfo
