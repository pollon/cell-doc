<!DOCTYPE html>
<html lang=en class="H(100%) M(0)">

<head>
	<meta name=viewport content="width=device-width, initial-scale=1">
	<title>Cell Reference</title>
	<link rel=stylesheet href=resources/normalizer.css />
	<link rel=stylesheet href=resources/style.css />
	<link rel=stylesheet href=resources/fonts/webfonts.css />
	<script src=resources/prelude.js ></script>
</head>

<body class="Bxz(bb) D(f) Fld(c) H(100%) M(0) Mih(100%) | Ff(Inconsolata) Fz(s1) Fz(s2)@sm Fz(s3)@md Lh(1.25) Tren(gp)">
	<header class="M(0) | Bdb(s,1px,#e4e4e4)">
		<div class="D(f) Flw(wr) M(a) Maw(64rem) W(100%)">
			<div class="D(f) Flx(1) | Bdy(s,1px,white) Bdb(s,1px,black):h">
				<!-- Search Icon -->
				<a href=/ >
					<div class="Flx(0,0) P(s03) P(s02)@md | Bgc(clear)">
						<svg class="W(s1)" viewBox="0 0 36 36">
							<circle cx=16 cy=16 r=14 fill=transparent stroke=#333 stroke-width=4 />
							<line x1=26 y1=26 x2=36 y2=36 stroke=#333 stroke-width=6 />
						</svg>
					</div> 
				</a>
				<input type=search id=search placeholder="Search css or class names here..." class="_rInput() Flx(1) P(s03) P(s02)@md | Bgc(clear) C(gray) C(black):f Ff(Inconsolata) Fz(s1) Fz(s2)@xs Fz(s3)@md Fw(b) Trsp(color) Trsdu(0.3s)" />
			</div>
			<div class="M(s04) P(s03) P(s02)@md | Ta(c)">
				<a href=/ class="Mx(s03) | C(grey) C(black):h Fw(n) Td(u) Lts(s05)">cells</a> 
				<a href=/about class="Mx(s03) | C(grey) C(black):h Fw(n) Td(u) Lts(s05)">about</a>
				<a href=/start class="Mx(s03) | C(grey) C(black):h Fw(n) Td(u) Lts(s05)">get started&rarr;</a>
			</div>
		</div>
	</header>
	<main class="Flx(a) | Ovx(h) Ovy(s)">
	<div class="Mx(a) Maw(60rem) | Bgc(white)">
