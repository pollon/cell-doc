{{> _beginPage}}

<div class="M(0) P(s2) P(s4)@md | Bgc(grey-10) C(grey-2)">
	<h1 class="M(0) | Fz(s3) Fz(s4)@md">Get started with cell css in three steps</h1>
	<p>
		<strong>1 - Clone</strong> 
		the cell repository 
		<a href=https://bitbucket.org/pollon/cell class="Mx(s03) | Bdb(s,1px,grey) C(black) C(grey):h Td(n) Lts(s05)">here&rarr;</a>
	</p>
	
	<p>
		<strong>2 - Setup</strong> 
	  cell by following the instructions in the
		<a href=https://bitbucket.org/pollon/cell class="Mx(s03) | Bdb(s,1px,grey) C(black) C(grey):h Td(n) Lts(s05)">readme&rarr;</a>
		<br>Or download a static binary:
		<a href=resources/dist/linux/cell class="Mx(s03) | Bdb(s,1px,grey) C(black) C(grey):h Td(n) Lts(s05)">Linux</a>
		<a href=resources/dist/osx/cell class="Mx(s03) | Bdb(s,1px,grey) C(black) C(grey):h Td(n) Lts(s05)">OSX</a>
	</p>

	<p>
		<strong>3 - Configure</strong> 
		cell by placing a
		<a href=http://yaml.org class="Bdb(s,1px,grey) C(black) C(grey):h Td(n) Lts(s05)">YAML</a>
		file named "cell.yaml" in the directory cell will be run
		from. (YAML is a human readable super-set of the ubiquitous JSON format.)
	</p>
	<h3 class="M(0) | Fz(s2) Fz(s3)@md">Here is an example config to get you started:</h1>
	<pre class="Bd(s,2px,grey-2)"><code id=config class=language-yaml></code></pre>
</div>

<link rel=stylesheet href=resources/prism.css />
<script src=resources/prism.js ></script>
<script>
$.get("text", "https://bitbucket.org/pollon/cell/raw/master/cell.yaml", function(t) {
	var c = $("#config")
	c.innerHTML = t;
	Prism.highlightElement(c);
});
</script>

{{> _endPage}}