{{> _beginPage}}

<div class="M(0) P(s1) P(s3)@md | Bgc(grey-10) C(grey-2)">
	<p class="M(0)">
		<strong class="Fw(b)">Cell css is for fast-as-thought web design</strong>
		without
		<a href="https://csswizardry.com/2014/10/the-specificity-graph/" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">specificity wars</a>,
		<a href="https://www.slideshare.net/stubbornella/css-bloat/5" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">bloat</a>,
		or
		<a href="https://www.sitepoint.com/css-framework-fortunes-failures-harry-roberts/" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">frameworks</a> &mdash;
		<a href="/start" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n) Whs(nw)">get started&rarr;</a>
		 or 
		<a href="/about" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n) Whs(nw)">read more...</a>
	</p>
</div>

<div id="list" class="M(.5rem) P(.5rem)">
</div>

<script>
var docstyle = {
	section: "Pb(1.25em)",
	article: "",
	h3: "Ff(Inconsolata) Fw(b)",
	h4: "Fw(n)",
	/* desc: "Maw(32rem) C(grey)", */
	doclink: "C(grey) Bdb(s,2px,grey-8) Ff(Inconsolata) Td(n) Lh(s3)",
	paragraph: "Mx(0) Mb(.01rem) Mt(0) Ff(i) Fz(1.2rem)@md",
	cell: "Ff(Inconsolata) Fw(n)",
	class: "C(grey) Ff(Inconsolata) Fw(n)",
	template: "C(grey) Ff(Inconsolata) Fw(n)"
};
</script>

<script src=resources/doc.js ></script>
<script src=resources/nerve.js ></script>
{{> _endPage}}
