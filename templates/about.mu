{{> _beginPage}}

<div class="M(0) P(s2) P(s4)@md | Bgc(grey-10) C(grey-2)">
	<div class="Pb(s5)">
		<h1 class="M(0) | Fw(b) Fz(s3) Fz(s4)@md">Web design in real-time with cell css</h1>
    <p>
    	<strong class="Fw(b)">Cell</strong> 
			is a css generator written in haskell.
		</p>

    <p>
      <strong class="Fw(b)">Cells</strong> 
			are
      <a href="https://www.slalom.com/thinking/rationalizing-functional-css" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">functional</a>,
      <a href="http://tachyons.io" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">single-purpose</a>
      classes. You write them with a standard syntax that 
      <strong class="Fw(b)">actually defines the property and value</strong>. 
    </p>
    
    <p>
      <strong class="Fw(b)">This means</strong>
			styling an element is as simple as changing the class name and cell regenerates your stylesheet.
    </p>

    <p class="Mb(0)">
      <strong class="Fw(b)">No need to</strong>:
    </p>
			<ul class="Mt(0) Mx(0) P(0) | List(n)">
				<li><span class="C(red)">&times;</span> invent a class name</li>
				<li><span class="C(red)">&times;</span> open a `&nbsp;.css` file to edit a class</li>
				<li><span class="C(red)">&times;</span> open a `&nbsp;.css` file to find out what a class does</li>
			</ul>
    
    <p>
      <strong class="Fw(b)">Cell is for fast-as-thought web design</strong>
			without
      <a href="https://csswizardry.com/2014/10/the-specificity-graph/" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">specificity wars</a>,
			<a href="https://www.slideshare.net/stubbornella/css-bloat/5" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">bloat</a>,
			or
      <a href="https://www.sitepoint.com/css-framework-fortunes-failures-harry-roberts/" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">frameworks</a> &mdash;
      <a href="/start" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">get started&rarr;</a>
       or
			<a href="/" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">search for specific classes&uarr;</a></p>
    </p>

    <p>
			<strong class="Fw(b)">Cell owes huge credit to</strong>:
			<a href="http://acss.io" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">Atomic CSS</a>,
			<a href="http://tachyons.io/" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">Tachyons CSS</a>,
			<a href="http://basscss.com/" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">Basscss</a>, 
			<a href="http://csswizardry.com/" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">CSS Wizardry</a>,
			<a href="http://cssmojo.com/" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">CSSMOJO</a>,
			and everyone doing their damndest to make web design accessible.
		</p>

    <p>
	    <strong class="Fw(b)">Cell was built by 
			<a href="http://pollon.io" class="Bdb(s,1px,grey) C(black) C(grey):h Td(n)">pollon</a></strong>.
		  We believe good work is a byproduct of good conversations.
		</p>
	</div>
</div>

{{> _endPage}}