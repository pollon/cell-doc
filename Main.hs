{-# LANGUAGE TemplateHaskell #-}
module Main where

import qualified Data.Text.Lazy    as LT (toStrict)
import qualified Data.Text.Lazy.IO as LT (putStrLn)
import Data.Aeson ((.=), object)
import Web.Spock
import Web.Spock.Config
import Text.Mustache (renderMustache, Template)
import Text.Mustache.Compile.TH (compileMustacheFile, compileMustacheDir')
import qualified Network.Wai.Middleware.Static as WAI (static)
import System.FilePath (takeExtension)

data Session = EmptySession
data State = EmptyState

main :: IO ()
main = do 
	config <- defaultSpockCfg EmptySession PCNoDatabase EmptyState
	let port = 8080
	let welcome = $(compileMustacheFile "banner.mu")
	LT.putStrLn $ renderMustache welcome $ object [ "port" .= show port ]
	runSpockNoBanner port (spock config routes)

routes :: SpockM () Session State ()
routes = do 
	middleware WAI.static
	get root $ render $(compileMustacheDir' (\p -> takeExtension p == ".mu") "index" "templates/")
	get "about" $ render $(compileMustacheDir' (\p -> takeExtension p == ".mu") "about" "templates/")
	get "start" $ render $(compileMustacheDir' (\p -> takeExtension p == ".mu") "start" "templates/")

render :: Template -> ActionCtxT ctx (WebStateM () Session State) ()
render page = do
	html $ LT.toStrict $ renderMustache page $ object []
