function getStrDist(a, b) {
  var al = a.length;
  var bl = b.length;
  if (al === 0) return bl;
  if (bl === 0) return al;
  var matrix = [];
  var i;
  for (i = 0; i <= bl; i++) {
    matrix[i] = [i];
  }
  var j;
  for (j = 0; j <= al; j++) {
    matrix[0][j] = j;
  }
  // Fill in the rest of the matrix
  for (i = 1; i <= bl; i++) {
    for (j = 1; j <= al; j++) {
      if (b.charAt(i-1) == a.charAt(j-1)) {
        matrix[i][j] = matrix[i-1][j-1];
      } else {
        matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, // substitution
        Math.min(matrix[i][j-1] + 1, // insertion
        matrix[i-1][j] + 1)); // deletion
      }
    }
  }
  return matrix[bl][al];
};

function search(needle, haystack, props, softness) {
  softness = typeof softness !== 'undefined' ? softness : 2;
  var results = [];
  var passed;
  var score;
  var thing;
  for (var i=0; i < haystack.length; i++) {
    passed = false;
    for (var j=0; !passed && j < props.length; j++) {
      if (haystack[i][props[j]]) {
        var tokens = (haystack[i][props[j]]).split(' ');
        for (var k=0; k < tokens.length; k++) {
          s = getStrDist(needle, tokens[k])
          if (s <= softness) {
            thing = haystack[i];
            thing.score = s;
            results.push(thing);
            passed=true;break;
          }
        }
      }
    }
  }
  return results.sort(function(a, b){return a.score - b.score});
};

function displayDoc(listNode, css, style) {
  for (var i = 0; i < css.length; ++i) {
    var section = $("$section");
    section.className = style.section;
    var article = $("$article");
    article.className = style.article;
    var h3 = $("$h3");
    h3.className = style.h3;
    h3.innerText = css[i].name;
    var desc = $("$p");
    desc.className = style.desc;
    var paragraph = $("$p");
    paragraph.className = style.paragraph;
    var cell = $("$code");
    cell.className = style.cell;
    var cellTemplate = $("$code");
    cellTemplate.className = style.template;
    var cellName;
    var key = css[i]["key"]
    if (css[i].template.search("$") != -1) {
      if (css[i].values && css[i].values[0]) {
        var sig = css[i].values[0];
        cellName = css[i].key + "(" + sig +") ";
      } else {
        cellName = css[i].key + "(<Args>) ";
      }
    } else {
      cellName = css[i].key + "() ";
    }
    cell.textContent = cellName;
    cellTemplate.textContent = (css[i].template).replace(" $", " $");
   	//desc.innerText = css[i].desc || "";
    paragraph.appendChild(cell);
    paragraph.appendChild(cellTemplate);
    article.appendChild(paragraph);
    if (css[i].aliases) {
      var aliases = css[i].aliases;
      for (abbr in aliases) {
        var paragraph = $("$p");
        paragraph.className = style.paragraph;
        var cell = $("$code");
        cell.className = style.cell;
        var cellTemplate = $("$code");
        cellTemplate.className = style.class;
        var cellName = css[i].key + "(" + abbr + ") "
        var template = css[i].template.substring(0, css[i].template.search(" ")) + ' ' + aliases[abbr];
        cell.textContent = cellName;
        cellTemplate.textContent = template;
        paragraph.appendChild(cell);
        paragraph.appendChild(cellTemplate);
        article.appendChild(paragraph)
      }
    }
    section.appendChild(h3);
    var link = $("$a");
    link.href = "https://developer.mozilla.org/en-US/docs/Web/CSS/" + css[i].template.split(":")[0];
    link.innerText = css[i].desc || "";
    link.className = style.doclink;
    // section.appendChild(desc);
    var h4 = $("$h4");
    h4.className = style.h4;
    h4.appendChild(link);
    section.appendChild(h4);
    section.appendChild(article);
    listNode.appendChild(section);
  }
}

function searchDoc(listNode, css, style) {
  return function(e) {
    if (e.keyCode === 13) {
      while (listNode.hasChildNodes()) {
          listNode.removeChild(listNode.lastChild);
      }
      terms = e.target.value.split(" ");
      if (terms.length > 1) {
        var results = [];
        for (var i=0; i < terms.length; ++i) {
          results = results.concat(search(terms[i], css, ["key", "name"], 0));
        }
      } else {
        var results = (search(e.target.value, css, ["key", "name", "template"])).sort(function(a, b){
          return a.score - b.score
        });
      }
      displayDoc(listNode, results, style);
    }
  }
}

document.addEventListener("DOMContentLoaded", function(e){
	var list = document.getElementById("list");
	$.get("json", "/resources/css.json", function(cssobj) {
		var css = [];
		for (var key in cssobj) {
			cssobj[key].key = key;
			css.push(cssobj[key]);
		}
		displayDoc(list, css, docstyle);
		document.getElementById("search").onkeydown = searchDoc(list, css, docstyle);
	})
});
